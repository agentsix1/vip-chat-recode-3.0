package me.jeremy.vippluschat.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import me.jeremy.vippluschat.main;
import net.md_5.bungee.api.ChatColor;


public class listner implements Listener{
	
	static main plugin;
	public listner(main instance) {

		plugin = instance;

		}
	
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		String[] cmd = e.getMessage().split(" ");
		String chatName = plugin.dC.returnChatName(cmd[0].replace("/", ""));
		Integer chatID = plugin.chns.getChatID(chatName);
		
		try {
			if (!chatName.equalsIgnoreCase("")) {
				if (plugin.perms.permChat(p, chatID)) {
					if (plugin.perms.permAllowChat(p)) {
						e.setCancelled(true);
						String sentance = "";
						for (String word : cmd) {
							if (!word.equalsIgnoreCase(cmd[0])) {
								if (sentance.equalsIgnoreCase("")) {
									sentance = word;
								} else {
									sentance = sentance + " " + word;
								}
							}
						}
						
						for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
							if (plugin.perms.permChat(pl, chatID)) {
								pl.sendMessage(plugin.chatCmdsMSG.replaceChatVariables(sentance, chatName, pl));
							}
						}
						
					} else {
						p.sendMessage(plugin.getConfig().getString("Messages.no-permissions").replace("&", "�"));
						e.setCancelled(true);
					}
				} else {
					p.sendMessage(plugin.getConfig().getString("Messages.no-permissions").replace("&", "�"));
					e.setCancelled(true);
				}
			}
		} catch (NullPointerException ex) {
			
		}
	}
}
