package me.jeremy.vippluschat.configs;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;


import me.jeremy.vippluschat.main;



public class config {
	
    
	static main plugin;
	public config(main instance) {

		plugin = instance;

		}
	
	//--- Start of config.yml writing tools
	
	private FileConfiguration Config = null; //customConfig 
    private File configFile = null; //customConfigFile
    
    public void reload() {
        if (configFile == null) {
        	configFile = new File(plugin.getDataFolder(), "config.yml");
        }
        Config = YamlConfiguration.loadConfiguration(configFile);
        // Look for defaults in the jar
        Reader defConfigStream;
		try {
			defConfigStream = new InputStreamReader(plugin.getResource("config.yml"), "UTF8");
		
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            Config.setDefaults(defConfig);
        }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }
    
    public FileConfiguration get() {
        if (Config == null) {
            reload();
        }
        return Config;
    }
    
    public void save() {
        if (Config == null || Config == null) {
            return;
        }
        try {
        	get().save(configFile);
        } catch (IOException ex) {
        	plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }
    
    public void saveDefault() {
        if (configFile == null) {
        	configFile = new File(plugin.getDataFolder(), "config.yml");
        }
        if (!configFile.exists()) {            
        	plugin.saveResource("config.yml", false);
         }
    }
    
    //--- End of config.yml writing tools	
    
    
    //--- Start of chat.yml writing tools
    private FileConfiguration ConfigChat = null; //customConfig 
    private File configFileChat = null; //customConfigFile
    
    public void reloadChat() {
        if (configFileChat == null) {
        	configFileChat = new File(plugin.getDataFolder(), "chat.yml");
        }
        ConfigChat = YamlConfiguration.loadConfiguration(configFileChat);
        // Look for defaults in the jar
        Reader defConfigStream;
		try {
			defConfigStream = new InputStreamReader(plugin.getResource("chat.yml"), "UTF8");
		
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            ConfigChat.setDefaults(defConfig);
        }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }
    
    public FileConfiguration getChat() {
        if (ConfigChat == null) {
            reloadChat();
        }
        return ConfigChat;
    }
    
    public void saveChat() {
        if (ConfigChat == null || ConfigChat == null) {
            return;
        }
        try {
        	getChat().save(configFileChat);
        } catch (IOException ex) {
        	plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFileChat, ex);
        }
    }
    
    public void saveDefaultChat() {
        if (configFileChat == null) {
        	configFileChat = new File(plugin.getDataFolder(), "chat.yml");
        }
        if (!configFileChat.exists()) {            
        	plugin.saveResource("chat.yml", false);
         }
    }
    //--- End of chat.yml writing tools
    
    
    //--- Start of other yml writing tools
    
    private FileConfiguration ConfigOther = null; //customConfig 
    private File configFileOther = null; //customConfigFile
    
    public void reloadOther(String configName) {
        if (configFileOther == null) {
        	configFileOther = new File(plugin.getDataFolder(),configName + ".yml");
        }
        ConfigOther = YamlConfiguration.loadConfiguration(configFileOther);
        // Look for defaults in the jar
        Reader defConfigStream;
		try {
			defConfigStream = new InputStreamReader(plugin.getResource(configName + ".yml"), "UTF8");
		
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            ConfigOther.setDefaults(defConfig);
        }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }
    
    public FileConfiguration getOther(String configName) {
        if (ConfigOther == null) {
            reloadOther(configName);
        }
        return ConfigOther;
    }
    
    public void saveOther(String configName) {
        if (ConfigOther == null || ConfigOther == null) {
            return;
        }
        try {
        	getOther(configName).save(configFileOther);
        } catch (IOException ex) {
        	plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFileOther, ex);
        }
    }
    
    public void saveDefaultOther(String configName) {
        if (configFileOther == null) {
        	configFileOther = new File(plugin.getDataFolder(), configName + ".yml");
        }
        if (!configFileOther.exists()) {            
        	plugin.saveResource("config.yml", false);
         }
    }
    
    //--- End of other yml writing tools
	}
