package me.jeremy.vippluschat.chat;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import me.jeremy.vippluschat.main;
import me.jeremy.vippluschat.configs.config;

public class Chats {
	
	private main plugin;
	public Chats(main plugin) {
	    this.plugin = plugin;
	}
	
	public config cfg = new config(plugin);
	public HashMap<String, Integer> chtStr = new HashMap<>();
	public HashMap<Integer, String> chtInt = new HashMap<>();
	public HashMap<Integer, List<String>> cmds = new HashMap<>();
	public HashMap<Integer, String> permission = new HashMap<>();
	public HashMap<Integer, Boolean> enabled = new HashMap<>();
	public HashMap<Integer, Boolean> toggleIgnore = new HashMap<>();
	public HashMap<Integer, String> tag = new HashMap<>();
	public HashMap<Integer, String> layout = new HashMap<>();
	public HashMap<Integer, String> colourcodes = new HashMap<>();
	public HashMap<Integer, Boolean> toggleASC = new HashMap<>();
	public HashMap<Integer, Boolean> toggleASP = new HashMap<>();
	public HashMap<Integer, String> fileASC = new HashMap<>();
	public HashMap<Integer, String> fileASP = new HashMap<>();
	
	public String getChatName(Integer chat) {
		return chtInt.get(chat);
	}
	
	public Integer getChatID(String chat) {
		return chtStr.get(chat);
	}
	
	public List<String> getCommands(Integer chat) {
		return cmds.get(chat);
	}
	
	public List<String> getCommands(String chat) {
		return cmds.get(getChatID(chat));		
	}
	
	public String getPermission(Integer chat) {
		return permission.get(chat);
	}
	
	public String getPermission(String chat) {
		return permission.get(getChatID(chat));		
	}
	
	public Boolean getEnabled(Integer chat) {
		return enabled.get(chat);
	}
	
	public Boolean getEnabled(String chat) {
		return enabled.get(getChatID(chat));		
	}
	
	public Boolean getToggleIgnore(Integer chat) {
		return toggleIgnore.get(chat);
	}
	
	public Boolean getToggleIgnore(String chat) {
		return toggleIgnore.get(getChatID(chat));		
	}
	
	public String getTag(Integer chat) {
		return tag.get(chat);
	}
	
	public String getTag(String chat) {
		return tag.get(getChatID(chat));		
	}
	
	public String getLayout(Integer chat) {
		return layout.get(chat);
	}
	
	public String getLayout(String chat) {
		return layout.get(getChatID(chat));
	}
	
	public String getColourCodes(Integer chat) {
		return colourcodes.get(chat);
	}
	
	public String getColourCodes(String chat) {
		return colourcodes.get(getChatID(chat));		
	}
	
	public Boolean getToggleASC(Integer chat) {
		return toggleASC.get(chat);
	}
	
	public Boolean getToggleASC(String chat) {
		return toggleASC.get(getChatID(chat));		
	}
	
	public Boolean getToggleASP(Integer chat) {
		return toggleASP.get(chat);
	}
	
	public Boolean getToggleASP(String chat) {
		return toggleASP.get(getChatID(chat));		
	}
	
	public String getFileASC(Integer chat) {
		return fileASC.get(chat);
	}
	
	public String getFileASC(String chat) {
		return fileASC.get(getChatID(chat));		
	}
	
	public String getFileASP(Integer chat) {
		return fileASP.get(chat);
	}
	
	public String getFileASP(String chat) {
		return fileASP.get(getChatID(chat));		
	}
	
	
	
	@SuppressWarnings("unused")
	private void clearAllData()  {
		chtStr.clear();
		chtInt.clear();
		cmds.clear();
		permission.clear();
		enabled.clear();
		toggleIgnore.clear();
		tag.clear();
		layout.clear();
		colourcodes.clear();
		toggleASC.clear();
		toggleASP.clear();
		fileASC.clear();
		fileASP.clear();
	}
	
	public void clearData(String data) {
		switch (data) {
		case "cmds":
			cmds.clear();
			break;
		case "permission":
			permission.clear();
			break;
		case "enabled":
			enabled.clear();
			break;
		case "toggleIgnore":
			toggleIgnore.clear();
			break;
		case "tag":
			tag.clear();
			break;
		case "layout":
			layout.clear();
			break;
		case "colourcodes":
			colourcodes.clear();
			break;
		case "toggleASC":
			toggleASC.clear();
			break;
		case "toggleASP":
			toggleASP.clear();
			break;
		case "fileASC":
			fileASC.clear();
			break;
		case "fileASP":
			fileASP.clear();
			break;
		}
	}
	
	public void reloadAllData() {
		reloadData("cmds");
		reloadData("permission");
		reloadData("enabled");
		reloadData("toggleIgnore");
		reloadData("tag");
		reloadData("layout");
		reloadData("colourcodes");
		reloadData("toggleASC");
		reloadData("toggleASP");
		reloadData("fileASC");
		reloadData("fileASP");
		
		
	}
	
	public void reloadData(String data) {
		clearData(data);
		loadData(data);
	}
	
	public void saveAllData() {
		
	}
	
	public void saveData(String data) {
		switch (data) {
		case "cmds":
			break;
		case "permission":
			break;
		case "enabled":
			break;
		case "toggleIgnore":
			break;
		case "tag":
			break;
		case "layout":
			break;
		case "colourcodes":
			break;
		case "toggleASC":
			break;
		case "toggleASP":
			break;
		case "fileASC":
			break;
		case "fileASP":
			break;
		}
	}
	
	public void loadAllData() {
		Set<String> parents = cfg.getChat().getKeys(false);
		int i = 0;
		for (String a : parents) {
			//Setting chtStr Hashmap and chtInt Hashmap
			chtInt.put(i, cfg.getChat().getString(a + ".name"));
			chtStr.put(cfg.getChat().getString(a + ".name"), i);
			//Setting cmds Hashmap
			loadCmds(i);
			//Setting permission Hashmap
			loadPermission(i);
			//Setting enabled HashMap
			loadEnabled(i);
			//Settings toggleIgnore Hashmap
			loadToggleIgnore(i);
			//Settings tag Hashmap
			loadTag(i);
			//Settings layout Hashmap
			loadLayout(i);
			//Settings layout Hashmap
			loadColourCodes(i);
			//Settings toggleASC Hashmap
			loadToggleASC(i);
			//Settings toggleASP Hashmap
			loadToggleASP(i);
			//Settings fileASC Hashmap
			loadToggleASC(i);
			//Settings fileASC Hashmap
			loadToggleASP(i);
			i+=1;
			
			
		}
		
	}
	
	@SuppressWarnings("unused")
	public void loadData(String data) {
		Set<String> parents = cfg.getChat().getKeys(false);
		int i = 0;
		for (String a : parents) {
			switch (data) {
			case "cmds":
				loadCmds(i);
				break;
			case "permission":
				loadPermission(i);
				break;
			case "enabled":
				loadEnabled(i);
				break;
			case "toggleIgnore":
				loadToggleIgnore(i);
				break;
			case "tag":
				loadTag(i);
				break;
			case "layout":
				loadLayout(i);
				break;
			case "colourcodes":
				loadColourCodes(i);
				break;
			case "toggleASC":
				loadToggleASC(i);
				break;
			case "toggleASP":
				loadToggleASP(i);
				break;
			case "fileASC":
				loadFileASC(i);
				break;
			case "fileASP":
				loadFileASP(i);
				break;
			}	
			i++;
		}
	}
	
	
	
	public void loadCmds(Integer chat) { //This loads the command using a integer
		cmds.put(chat, cfg.getChat().getStringList(getChatName(chat) + ".command"));
	}
	
	public void loadCmds(String chat) { //This loads the command using a string
		cmds.put(getChatID(chat), cfg.getChat().getStringList(chat + ".command"));
	}
	
	public void loadPermission(Integer chat) {
		permission.put(chat, cfg.getChat().getString(getChatName(chat) + ".permission"));
	}
	
	public void loadPermission(String chat) {
		permission.put(getChatID(chat), cfg.getChat().getString(chat + ".permission"));
	}
	
	public void loadEnabled(Integer chat) {
		enabled.put(chat, cfg.getChat().getBoolean(getChatName(chat) + ".enabled"));
	}
	
	public void loadEnabled(String chat) {
		enabled.put(getChatID(chat), cfg.getChat().getBoolean(chat + ".enabled"));
	}
	
	public void loadToggleIgnore(Integer chat) {
		toggleIgnore.put(chat, cfg.getChat().getBoolean(getChatName(chat) + ".toggle-ignore"));
	}
	
	public void loadToggleIgnore(String chat) {
		toggleIgnore.put(getChatID(chat), cfg.getChat().getBoolean(chat + ".toggle-ignore"));
	}
	
	public void loadTag(Integer chat) {
		tag.put(chat, cfg.getChat().getString(getChatName(chat) + ".tag"));
	}
	
	public void loadTag(String chat) {
		tag.put(getChatID(chat), cfg.getChat().getString(chat + ".tag"));
	}
	
	public void loadLayout(Integer chat) {
		layout.put(chat, cfg.getChat().getString(getChatName(chat) + ".layout"));
	}
	
	public void loadLayout(String chat) {
		colourcodes.put(getChatID(chat), cfg.getChat().getString(chat + ".layout"));
	}
	
	public void loadColourCodes(Integer chat) {
		colourcodes.put(chat, cfg.getChat().getString(getChatName(chat) + ".color-codes"));
	}
	
	public void loadColourCodes(String chat) {
		colourcodes.put(getChatID(chat), cfg.getChat().getString(chat + ".color-codes"));
	}
	
	public void loadToggleASC(Integer chat) {
		toggleASC.put(chat, cfg.getChat().getBoolean(getChatName(chat) + ".antiswear.server.enabled"));
	}
	
	public void loadToggleASC(String chat) {
		toggleASC.put(getChatID(chat), cfg.getChat().getBoolean(chat + ".antiswear.server.enabled"));
	}
	
	public void loadToggleASP(Integer chat) {
		toggleASP.put(chat, cfg.getChat().getBoolean(getChatName(chat) + ".antiswear.player.enabled"));
	}
	
	public void loadToggleASP(String chat) {
		toggleASP.put(getChatID(chat), cfg.getChat().getBoolean(chat+ ".antiswear.player.enabled"));
	}
	
	public void loadFileASC(Integer chat) {
		fileASC.put(chat, cfg.getChat().getString(getChatName(chat) + ".antiswear.server.file-name"));
	}
	
	public void loadFileASC(String chat) {
		fileASC.put(getChatID(chat), cfg.getChat().getString(chat + ".antiswear.server.file-name"));
	}
	
	public void loadFileASP(Integer chat) {
		fileASP.put(chat, cfg.getChat().getString(getChatName(chat) + ".antiswear.player.file-name"));
	}
	
	public void loadFileASP(String chat) {
		fileASP.put(getChatID(chat), cfg.getChat().getString(chat + ".antiswear.player.file-name"));
	}
	
	
}
