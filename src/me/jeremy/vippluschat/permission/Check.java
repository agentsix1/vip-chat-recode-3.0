package me.jeremy.vippluschat.permission;

import org.bukkit.entity.Player;

import me.jeremy.vippluschat.main;

public class Check {
	static main plugin;
	public Check(main instance) {

		plugin = instance;

		}
	
	/*
	 * Permission Checker For Chat
	 */
	public Boolean permChat(Player p, Integer chat) {
		if (p.hasPermission("vippluschat.chat.*")) {
			return true;
		}
		return p.hasPermission("vippluschat.chat." + plugin.chns.getPermission(chat));
	}
	
	public Boolean permChat(Player p, String chat) {
		if (p.hasPermission("vippluschat.chat.*")) {
			return true;
		}
		return p.hasPermission("vippluschat.chat." + plugin.chns.getPermission(chat));
	}

	public Boolean permAllowChat(Player p) {
		return p.hasPermission("vippluschat.allowchat");
	}
	
}
