# VIP+ Chat v1.0 Complete Recode Status
This is the page where I will be posting all updates refering to this project

## Features that have been added

- Custom Chat Layout
- Chats have custom commands
- Chats have custom permission nodes
- Custom Chats
- The old number system is no longer required and you can now set the names in the chat.yml! ![missing](https://cdn2.iconfinder.com/data/icons/ledicons/new.png)
- Custom chats have multiple commands

### Features To Be Added
- Ability to disable color codes
- Toggle color codes in game
- Toggle chat state
- Blacklist system
- Check version command
- Ignore System
- Custom Messages
- Focus System
- Antiswear System
- Possibly more if I want to add more.